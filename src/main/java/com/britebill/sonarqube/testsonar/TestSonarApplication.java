package com.britebill.sonarqube.testsonar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Random;

@SpringBootApplication
public class TestSonarApplication {

	Person joseCarlos;

	public TestSonarApplication(){
		 joseCarlos = new Person("Jose Carlos", "De Frutos", "josecarlos.defrutos@amdocs.com", "609154268", 37);
	}

	public static void main(String[] args) {
		SpringApplication.run(TestSonarApplication.class, args);
	}

	public boolean firstGTSecond(int a, int b){
		return a > b;
	}

	public int getRandomInteger(boolean pos){
		if (pos){
			return new Random().nextInt();
		} else {
			return 0;
		}
	}

	public int getRandomStringPlusOne(){
		return new Random().nextInt() + 1;
	}

	public int getRandomStringPlusTwo(){
		return new Random().nextInt() + 2;
	}

	public int newBirthdayForPersons(){
//		return joseCarlos.
		return 0;
	}

}
