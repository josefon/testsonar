package com.britebill.sonarqube.testsonar;

public class Person {
    private String name;
    private String surname;
    private String email;
    private String telephone;
    private int age;

    public Person(){
        this.name="";
        this.surname="";
        this.email="";
        this.telephone="";
        this.age=0;
    }

    public Person(String name, String surname, String email, String telephone, int age){
        this.name=name;
        this.surname=surname;
        this.email=email;
        this.telephone=telephone;
        this.age=age;
    }

    public int newBirtdhay(){
        this.age = this.age + 1;
        return this.age;
    }

}