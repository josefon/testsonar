package com.britebill.sonarqube.testsonar;

import org.junit.After;
import org.junit.Before;
import org.junit.jupiter.api.Test;

import org.junit.Assert;

import static org.hamcrest.CoreMatchers.is;

//@SpringBootTest
//@RunWith(Junit4.class)
class TestSonarApplicationTests {

	TestSonarApplication testSonarApplication = new TestSonarApplication();

	@Before
	public void init(){
//		testSonarApplication = new TestSonarApplication();
	}

	@After
	public void finish(){
//		testSonarApplication = null;
	}

	@Test
	void contextLoads() {
	}

	@Test
	public void testFirstGTSecondIsTrue(){
//		TestSonarApplication testSonarApplication = new TestSonarApplication();
		Assert.assertThat(testSonarApplication.firstGTSecond(2, 1), is(true));
	}

	@Test
	public void testFirstGTSecondIsFalse(){
//		TestSonarApplication testSonarApplication = new TestSonarApplication();
		Assert.assertThat(testSonarApplication.firstGTSecond(1, 2), is(false));
	}

	@Test
	public void testMain(){
		String[] args = new String[1];
		args[0] = "a";
 		TestSonarApplication.main(args);
	}

/*	@Test
	public void testGetRandomInteger(){
//		TestSonarApplication testSonarApplication = new TestSonarApplication();
		int a = testSonarApplication.getRandomInteger(false);
		Assert.assertEquals(a, 0);

		a = testSonarApplication.getRandomInteger(true);
		Assert.assertNotEquals(a, 0);

	}*/

}
